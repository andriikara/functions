const getSum = (str1, str2) => {
  if (!/^\d+$/.test(str1) || !/^\d+$/.test(str2)) {
    if (typeof str1 === 'string' && str1.length === 0) {
      return str2;
    }
    return false;
  }
  else if (str1.length < 7 || str2.length < 7) {
    let result = Number(str1) + Number(str2);
    return result.toString();
  }
  else {
      let result = BigInt(str1) + BigInt(str2);
      return result.toString();
    }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const posts = listOfPosts.filter(n => n.author === authorName);
  let comments = 0;
  const filt = Array.from(listOfPosts.filter(n=>n.comments !== undefined), n=>n.comments);
  
  for (const ar of filt) {
    comments += ar.filter(n=>n.author === authorName).length;
  }

  return `Post:${posts.length},comments:${comments}`;
};

const tickets=(people)=> {
  let cash = 0;
  for (const bill of people)
  {
    if (bill === 25) cash+= bill;
    else{
      cash-= bill-25;
      if (cash < 0) return 'NO';
      cash+= bill;
    } 
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
